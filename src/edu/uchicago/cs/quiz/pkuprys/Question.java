package edu.uchicago.cs.quiz.pkuprys;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Question implements Serializable {

    private static final long serialVersionUID = 6546546516546843135L;

    private String mForeign;
    private String mEnglish;
    private String mLanguage;

    private Set<String> wrongAnswers = new HashSet<String>();

    public Question(String mForeign, String mEnglish, String mLanguage) {
        this.mForeign = mForeign;
        this.mEnglish = mEnglish;
        this.mLanguage = mLanguage;
    }

    public Question(String[] strArray) {
        if(strArray.length == 3) {
            this.mForeign = strArray[0];
            this.mEnglish = strArray[1];
            this.mLanguage = strArray[2];
        }
    }

    //getter and setter methods for the all the variables in this class
    public String getmForeign() {
        return mForeign;
    }

    public void setmForeign(String mForeign) {
        this.mForeign = mForeign;
    }

    public String getmEnglish() {
        return mEnglish;
    }

    public void setmEnglish(String mEnglish) {
        this.mEnglish = mEnglish;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public Set<String> getWrongAnswers() {
        return wrongAnswers;
    }

    public boolean addWrongAnswer(String wrongAnswer){
        return wrongAnswers.add(wrongAnswer);
    }

    public String getQuestionText(){
        return "Which is " + mEnglish + "?";
    }
}
