package edu.uchicago.cs.quiz.pkuprys;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class QuestionActivity extends Activity {
    public static final String QUESTION = "edu.uchicago.cs.quiz.pkuprys.QUESTION";
    public static final String RADS = "edu.uchicago.cs.quiz.pkuprys.RADS";


    private static final String DELIMITER = "\\|";
    private static final int NUM_ANSWERS = 5;

    private static final int FOR = 0;
    private static final int ENG = 1;
    private static final int LAN = 2;

    Random mRandom;

    private Question mQuestion;
    private String[] mWords;
    private boolean mItemSelected = false;
    private RadioButton[] mRadioButtons;

    //make these members
    TextView mQuestionNumberTextView;
    RadioGroup mQuestionRadioGroup;
    TextView mQuestionTextView;
    Button mSubmitButton;
    Button mQuitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        //generate a question
        mWords = getResources().getStringArray(R.array.word_classics);

        //get refs to inflated members
        mQuestionNumberTextView = (TextView) findViewById(R.id.questionNumber);
        mQuestionTextView = (TextView) findViewById(R.id.questionText);
        mSubmitButton = (Button) findViewById(R.id.submitButton);

        //init the random
        mRandom = new Random();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });


        //set quit button action
        mQuitButton = (Button) findViewById(R.id.quitButton);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayResult();
            }
        });

        mQuestionRadioGroup = (RadioGroup) findViewById(R.id.radioAnswers);
        //disallow submitting until an answer is selected
        mQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mSubmitButton.setEnabled(true);
                mItemSelected = true;
            }
        });

        fireQuestion(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //pass the question into the bundle when I have a config change
        outState.putSerializable(QuestionActivity.QUESTION, mQuestion);
        mQuestionRadioGroup.removeAllViews();
        outState.putSerializable(QuestionActivity.RADS, mRadioButtons);
    }


    private void fireQuestion(){
        populateUserInterface();
    }

    //overloaded to take savedInstanceState
    private void fireQuestion(Bundle savedInstanceState){

        if (savedInstanceState == null ){
            populateUserInterface();
        } else {
            populateUserInterface(savedInstanceState);
        }

    }

    //populate the question and the choices when a saved instance state is not available
    private void populateUserInterface() {
        mQuestion = generateGoodQuestion(QuizTracker.getInstance().getLanguage());
        //take care of button first
        mSubmitButton.setEnabled(false);
        mItemSelected = false;

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        //will generate a number 0-4 inclusive
        int randomPosition = mRandom.nextInt(NUM_ANSWERS);
        int counter = 0;

        mRadioButtons = new RadioButton[NUM_ANSWERS];

        mQuestionRadioGroup.removeAllViews();
        //for each of the 5 wrong answers
        for (String wrongAnswer : mQuestion.getWrongAnswers()) {
            if (counter == randomPosition) {
                //insert the cor answer
                mRadioButtons[counter] = addRadioButton(mQuestionRadioGroup, mQuestion.getmForeign());
            } else {
                mRadioButtons[counter] = addRadioButton(mQuestionRadioGroup, wrongAnswer);

            }
            counter++;
        }
    }

    //populate the question and the choices when a saved instance state is available
    private void populateUserInterface(Bundle savedInstanceState) {
        mQuestion = (Question) savedInstanceState.getSerializable(QuestionActivity.QUESTION);
        //take care of button first
        mRadioButtons = (RadioButton[]) savedInstanceState.getSerializable(QuestionActivity.RADS);

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        for (int nC = 0; nC < mRadioButtons.length; nC++) {
                mQuestionRadioGroup.addView(mRadioButtons[nC]);
        }

    }

        private Question generateGoodQuestion(byte yLanguage) {

            mQuestion = getRandomQuestion(yLanguage);

            while (mQuestion.getWrongAnswers().size() < NUM_ANSWERS) {
                Question wrongQuestion;
                do {
                   wrongQuestion = getRandomQuestion(yLanguage);
                } while (mQuestion.getmEnglish().equals(wrongQuestion.getmEnglish()));

                mQuestion.addWrongAnswer(wrongQuestion.getmForeign());
            }

            return mQuestion;
        }


        //get the question based on the language of the question
        private Question getRandomQuestion(byte yLanguage) {
            int index;
            String[] strForEngLans;
            Question question;

            switch (yLanguage) {
                case QuizTracker.LATIN:
                    do {
                        index = mRandom.nextInt(mWords.length);
                        strForEngLans = mWords[index].split(DELIMITER);
                        question = new Question(strForEngLans);
                    } while (!question.getmLanguage().equals("LAT"));

                    return question;

                case QuizTracker.GREEK:
                    do {
                        index = mRandom.nextInt(mWords.length);
                        strForEngLans = mWords[index].split(DELIMITER);
                        question = new Question(strForEngLans);
                    } while (!question.getmLanguage().equals("GRK"));

                    return question;

                case QuizTracker.MIXED:
                default:
                    index = mRandom.nextInt(mWords.length);
                    return new Question(mWords[index].split(DELIMITER));
            }
        }

    //check the answer as well as increasing the question number
    private void submit() {

        Button checkedButton = (Button) findViewById(mQuestionRadioGroup.getCheckedRadioButtonId());
        String guess = checkedButton.getText().toString();
        //see if they guessed right
        if (mQuestion.getmForeign().equals(guess)) {
            QuizTracker.getInstance().answeredRight();
        } else {
            QuizTracker.getInstance().answeredWrong();
        }
        if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
            //increment the question number
            QuizTracker.getInstance().incrementQuestionNumber();
            fireQuestion();
        } else {
            displayResult();
        }

     }

    //move to the result page and finish the current
    private void displayResult(){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuQuit:
                displayResult();
                return true;
            case R.id.menuSubmit:
                if(mItemSelected){
                submit();
                }
                else{
                    Toast toast = Toast.makeText(this, getResources().getText(R.string.pleaseSelectAnswer), Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //return reference button
    private RadioButton addRadioButton(RadioGroup questionGroup, String text) {
        RadioButton button = new RadioButton(this);
        button.setText(text);
        button.setTextColor(Color.WHITE);
        questionGroup.addView(button);
        return button;

    }

}
